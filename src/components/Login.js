import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = { gebruikersnaam: "", wachtwoord: "" };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    alert(
      "Ontvangen gebruikersnaam en wachtwoord: " +
        this.state.gebruikersnaam +
        " " +
        this.state.wachtwoord
    );
    event.preventDefault();
  }

  render() {
    return (
      <div className="pagina-titel">
        <h1>Login</h1>
        <div className="container">
          <div className="row justify-content-center m-2 pagina-blok">
            <form onSubmit={this.handleSubmit} autoComplete="on">
              <div className="form-group mt-4">
                <div className="input-group mb-3">
                  <div className="input-group-prepend">
                    <span className="input-group-text" id="add-icon">
                      <FontAwesomeIcon icon={["fas", "hard-hat"]} />
                    </span>
                    <input
                      name="gebruikersnaam"
                      value={this.state.gebruikersnaam}
                      onChange={this.handleInputChange}
                      id="gebruikersnaam"
                      placeholder="gebruikersnaam"
                      className="form-control"
                      type="text"
                      aria-describedby="add-icon"
                      autoFocus
                      required
                    />
                  </div>
                </div>
              </div>

              <div className="form-group">
                <div className="input-group mb-3">
                  <div className="input-group-prepend">
                    <span className="input-group-text" id="add-icon1">
                      <FontAwesomeIcon icon={["fas", "wrench"]} />
                    </span>
                    <input
                      name="wachtwoord"
                      value={this.state.wachtwoord}
                      onChange={this.handleInputChange}
                      id="wachtwoord"
                      placeholder="wachtwoord"
                      className="form-control"
                      type="password"
                      aria-describedby="add-icon1"
                      autoComplete="off"
                      required
                    />
                  </div>
                </div>
              </div>

              <button className="btn btn-warning mt-2 mb-2">Inloggen</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
