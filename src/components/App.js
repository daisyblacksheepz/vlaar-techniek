import React from "react";
// import logo from "../logo.gif";
import "../styles/App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Login from "./Login";
import CheckIn from "./CheckIn";
import ProductScanner from "./ProductScanner";
import Overzicht from "./Overzicht";
import Afronding from "./Afronding";

function App() {
  const NoMatchPage = () => {
    return <h3 style={{ color: "white" }}>404 - Not found</h3>;
  };

  return (
    <Router>
      <div className="App mb-4">
        <header className="App-header">
          {/* <img
            style={{
              width: "10%",
              height: "auto"
            }}
            src={logo}
            className="App-logo"
            alt="logo"
          /> */}
          <h1>Vlaar</h1>
          <h2>Techniek</h2>
        </header>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/checkin" component={CheckIn} />
          <Route exact path="/scanner" component={ProductScanner} />
          <Route exact path="/overzicht" component={Overzicht} />
          <Route exact path="/afronding" component={Afronding} />
          <Route component={NoMatchPage} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
