import React from "react";
import { Cameras, Scanner, onScan } from "react-instascan";
import Clock from 'react-live-clock';

class CheckIn extends React.Component {

  render() {
    return (
      <div className="pagina-titel">
        <h1>Inchecken Locatie</h1>
        <div className="container">
          <div className="row justify-content-center m-2 pagina-blok">
            <form onSubmit={this.handleSubmit} autoComplete="on">
              <div className="scanner">
                <Cameras>
                  {cameras => (
                    <div>
                      <h2>Scan de locatie QRcode!</h2>
                      <Scanner camera={cameras[0]} onScan={onScan}>
                        <video style={{ width: 400, height: 400 }} />
                      </Scanner>
                    </div>
                  )}
                </Cameras>
              </div>
              <div className="tijd"><h4>Datum & Tijd</h4>
              <Clock
        ticking={true}
        format={'dddd, D MMMM YYYY, h:mm:ss A'}
        timezone={'Europe/Amsterdam'} />
              </div>
              <button className="btn btn-light mt-2 mb-2">Uitloggen</button>
              <button className="btn btn-warning ml-2 mt-2 mb-2">
                Product Scanner
              </button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default CheckIn;
