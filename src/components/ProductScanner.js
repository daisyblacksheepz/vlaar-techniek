import React from "react";
import { Cameras, Scanner, onScan } from "react-instascan";
import Timer from "react-compound-timer";

class ProductScanner extends React.Component {
  render() {
    return (
      <div className="pagina-titel">
        <h1>Product Scanner</h1>
        <div className="container">
          <div className="col justify-content-center m-2 pagina-blok">
            <div className="row justify-content-center scanner">
              <Cameras>
                {cameras => (
                  <div>
                    <h2>Scan de QRcode van producten</h2>
                    <Scanner camera={cameras[0]} onScan={onScan}>
                      <video style={{ width: 400, height: 400 }} />
                    </Scanner>
                  </div>
                )}
              </Cameras>
            </div>
            <div className="row justify-content-center tijd">
              <h3 className="col-12">Gewerkte Tijd</h3>
              <Timer startImmediately={false}>
                {({ start, resume, pause, stop, reset }) => (
                  <React.Fragment>
                    <div className="col-12">
                      <Timer.Days /> days ,
                      <Timer.Hours /> hours ,
                      <Timer.Minutes /> minutes ,
                      <Timer.Seconds /> seconds
                    </div>
                    <br />
                    <div className="col-12">
                      <button onClick={start}>Start</button>
                      <button onClick={pause}>Pauze</button>
                      <button onClick={resume}>Hervat</button>
                      <button onClick={stop}>Stop</button>
                      <button onClick={reset}>Reset</button>
                    </div>
                  </React.Fragment>
                )}
              </Timer>
            </div>
            <button className="btn btn-light mt-2 mb-2">Uitloggen</button>
            <button className="btn btn-warning ml-2 mt-2 mb-2">
              Product Scanner
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductScanner;
