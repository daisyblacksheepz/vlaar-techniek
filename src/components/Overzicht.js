import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import SignatureCanvas from "react-signature-canvas";

class Overzicht extends React.Component {
  constructor(props) {
    super(props);
    this.state = { werknemer: "", klantnaam: "", locatie: "" };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    alert(
      "Ontvangen werknemer, klantnaam en locatie: " +
        this.state.werknemer +
        " " +
        this.state.klantnaam +
        " " +
        this.state.locatie
    );
    event.preventDefault();
  }

  sigPad = {};
  clear = () => {
    this.sigPad.clear();
  };

  render() {
    return (
      <div className="pagina-titel">
        <h1>Overzicht</h1>
        <div className="container">
          <div className="row justify-content-center m-2 pagina-blok">
            <div className="col-12">
              <div className="row justify-content-center">
                <form onSubmit={this.handleSubmit} autoComplete="on">
                  <div className="form-group mt-4">
                    <div className="input-group mb-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text" id="add-icon">
                          <FontAwesomeIcon icon={["fas", "user-tie"]} />
                        </span>
                        <input
                          name="klantnaam"
                          value={this.state.klantnaam}
                          onChange={this.handleInputChange}
                          id="klantnaam"
                          placeholder="naam bedrijf/klant"
                          className="form-control"
                          type="text"
                          aria-describedby="add-icon"
                          autoFocus
                          required
                        />
                      </div>
                    </div>
                  </div>

                  <div className="form-group mt-4">
                    <div className="input-group mb-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text" id="add-icon">
                          <FontAwesomeIcon icon={["fas", "hard-hat"]} />
                        </span>
                        <input
                          name="werknemer"
                          value={this.state.werknemer}
                          onChange={this.handleInputChange}
                          id="werknemer"
                          placeholder="werknemer"
                          className="form-control"
                          type="text"
                          aria-describedby="add-icon"
                          autoFocus
                          disabled
                        />
                      </div>
                    </div>
                  </div>

                  <div className="form-group mt-4">
                    <div className="input-group mb-3">
                      <div className="input-group-prepend">
                        <span className="input-group-text" id="add-icon">
                          <FontAwesomeIcon icon={["fas", "globe-europe"]} />
                        </span>
                        <input
                          name="locatie"
                          value={this.state.locatie}
                          onChange={this.handleInputChange}
                          id="locatie"
                          placeholder="locatie"
                          className="form-control"
                          type="text"
                          aria-describedby="add-icon"
                          autoFocus
                          disabled
                        />
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div className="row justify-content-center factuur">
                <div className="col-8 mb-2 mt-2 diensten">
                  <h4>Factuur</h4>
                  <div className="row totaalprijs">
                    <div className="col-4" style={{ textAlign: "end" }}>
                      Diensten
                    </div>
                    <div className="col-2"></div>
                    <div className="col-3" style={{ textAlign: "end" }}>
                      Aantal
                    </div>
                    <div className="col-3">Prijs</div>
                  </div>
                  <div className="row reiskosten">
                    <div className="col-4" style={{ textAlign: "end" }}>
                      Reis- en transportkosten
                    </div>
                    <div className="col-2"></div>
                    <div className="col-3" style={{ textAlign: "end" }}></div>
                    <div className="col-3">€12,-</div>
                  </div>
                </div>
                <div className="col-8 mb-2 mt-2 producten">
                  <div className="row productenlijst">
                    <div className="col-4" style={{ textAlign: "end" }}>
                      Producten
                    </div>
                    <div className="col-2"></div>
                    <div className="col-3" style={{ textAlign: "end" }}></div>
                    <div className="col-3"></div>
                  </div>
                  <div className="row gescandeproducten">
                    <div className="col-4" style={{ textAlign: "end" }}>
                      productnaam
                    </div>
                    <div className="col-2"></div>
                    <div className="col-3" style={{ textAlign: "end" }}>
                      1
                    </div>
                    <div className="col-3">€15,-</div>
                  </div>
                  <div className="row totaalprijs">
                    <div className="col-4" style={{ textAlign: "end" }}>
                      Totale kosten
                    </div>
                    <div className="col-2"></div>
                    <div className="col-3" style={{ textAlign: "end" }}></div>
                    <div className="col-3">€343</div>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-12 mb-2 mt-2 handtekening">
                  <h4>Handtekening</h4>
                  <div>
                    <SignatureCanvas
                      penColor="white"
                      canvasProps={{
                        width: 500,
                        height: 200,
                        className: "sigCanvas"
                      }}
                      ref={ref => {
                        this.sigPad = ref;
                      }}
                    />
                  </div>
                  <button className="btn btn-danger" onClick={this.clear}>
                    Clear
                  </button>
                </div>
              </div>
              <div className="row">
                <div className="col-12 navbuttons">
                  <button className="btn btn-light mt-2 mb-2">Uitloggen</button>
                  <button className="btn btn-secondary ml-2 mt-2 mb-2">
                    Product Scanner
                  </button>
                  <button className="btn btn-warning ml-2 mt-2 mb-2">
                    Akkoord
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Overzicht;
