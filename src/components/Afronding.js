import React from "react";

class Afronding extends React.Component {
  render() {
    return (
      <div className="pagina-titel">
        <h1>Afronding</h1>
        <div className="container">
          <div className="row justify-content-center m-2 pagina-blok">
            <div className="col-12 info">
              <h4>
                Indien u op afronden klikt, gaat u akkoord met de opgestelde
                factuur. Deze word na afronding middels de mail verstreken.
              </h4>
            </div>

            <div className="row">
              <div className="col-12 navbuttons">
                <button className="btn btn-secondary ml-2 mt-2 mb-2">
                  Overzicht
                </button>
                <button className="btn btn-warning ml-2 mt-2 mb-2">
                  Afronden
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Afronding;
